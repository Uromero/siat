<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clase_horario extends Model
{
    use HasFactory;

    protected $table = 'clase_horarios';

    protected $fillable = [

      'materia_profesores_id',
      'fecha_inicio',
      'fecha_final',
      'cupo',

    ];

}
