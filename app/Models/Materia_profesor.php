<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materia_profesor extends Model
{
    use HasFactory;

    protected $table = 'materia_profesores';

    protected $fillable = [

        'materias_id',
        'profesores_id',

    ];

}
