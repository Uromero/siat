<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Clase_horario_dia_hora extends Model
{
    use HasFactory;

    protected $table = 'clase_horario_dia_hora';

    protected $fillable = [

        'clase_horarios_id',
        'dias_id',
        'horas_id',

    ];

}
