<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Materia_profesor_alumno extends Model
{
    use HasFactory;

    protected $table = 'materia_profesor_alumnos';

    protected $fillable = [

        'materia_profesores_id',
        'numero',
        'users_id'

    ];

}
