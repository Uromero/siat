<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClaseHorariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clase_horarios', function (Blueprint $table) {
            $table->id();
            $table->foreignId('materia_profesores_id')->constrained();
            $table->date('fecha_inicio');
            $table->date('fecha_final');
            $table->integer('cupo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clase_horarios');
    }
}
