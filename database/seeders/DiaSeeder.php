<?php

namespace Database\Seeders;

use App\Models\Dia;
use Illuminate\Database\Seeder;

class DiaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Dia::insert ([

           [

               'nombre' => 'Lunes',
               'habilitado' => 'true',

           ]

        ]);

    }
}
