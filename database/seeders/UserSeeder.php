<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::insert(
            [
                [
                    'nombre' => 'uriel indalecio',
                    'apellidos' => 'cazuelas rodriguez',
                    'email' => 'email@prueba.com',
                    'codigo' => '2222222222',
                    'nip' => 'abc123',
                ],
                [
                    'nombre' => 'frijolito indalecio',
                    'apellidos' => 'pocillos rodriguez',
                    'email' => 'emaila@pruebaa.com',
                    'codigo' => '1111111111',
                    'nip' => 'abc456',
                ]
            ]
        );
    }
}
