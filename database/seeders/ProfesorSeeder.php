<?php

namespace Database\Seeders;

use App\Models\Profesor;
use Illuminate\Database\Seeder;

class ProfesorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Profesor::insert([

            [
                'nombre' => 'Ruperto',
                'apellidos' => 'Ruedas Alvares',
                'codigo' => '3333333333',
                'nip' => 'abc789',
                'email' => 'maestro@prueba.com',
            ]


        ]);
    }
}
