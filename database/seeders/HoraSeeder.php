<?php

namespace Database\Seeders;

use App\Models\Hora;
use Illuminate\Database\Seeder;

class HoraSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Hora::insert ([

            'hora' => '07:00',
            'habilitado' => 'true',

        ]);

    }
}
