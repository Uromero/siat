<?php

namespace Database\Seeders;

use App\Models\Materia;
use Illuminate\Database\Seeder;

class MateriaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Materia::insert ([

           [
               'clave' => '9999',
               'nombre' => 'empanadas',
           ]

        ]);

    }
}
